import 'package:flutter/material.dart';
import 'home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: _themeData,
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

final ThemeData _themeData = _buildTheme();

ThemeData _buildTheme() {
  final ThemeData base = ThemeData.dark();

  return base.copyWith(
    floatingActionButtonTheme:
        FloatingActionButtonThemeData(backgroundColor: Colors.white),
  );
}
