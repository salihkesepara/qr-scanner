import 'dart:async';

import 'package:flutter/services.dart';

/// Scan mode which is either QR code or BARCODE
enum ScanMode { QR, BARCODE, DEFAULT }

/// Flutter barcode scanner class that bridge the native classes to flutter project
class FlutterBarcodeScanner {
  /// Create a method channel instance
  static const MethodChannel _channel =
      const MethodChannel('flutter_barcode_scanner');

  static Future<String> scanBarcode({
    String lineColor = '#ff6666',
    String cancelButtonText = 'Cancel',
    bool isShowFlashIcon = true,
    ScanMode scanMode = ScanMode.QR,
  }) async {
    Map params = <String, dynamic>{
      "lineColor": lineColor,
      "cancelButtonText": cancelButtonText,
      "isShowFlashIcon": isShowFlashIcon,
      "isContinuousScan": false,
      "scanMode": scanMode.index
    };

    String barcodeResult = await _channel.invokeMethod('scanBarcode', params);
    if (null == barcodeResult) {
      barcodeResult = "";
    }
    
    return barcodeResult;
  }
}
