import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'barcode_scanner.dart';
import 'package:url_launcher/url_launcher.dart';
import 'floating_action_buttons.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String _text = '';

  @override
  void initState() {
    Future.delayed(const Duration(seconds: 1), scanBarcode);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('QR Code Scanner')),
        floatingActionButton: FloatingActionButtons(
          text: _text,
          scanBarcode: scanBarcode,
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                _text,
                style: Theme.of(context)
                    .textTheme
                    .body2
                    .copyWith(color: Colors.white),
              ),
            ),
          ],
        ),
      );
  }

  Future<void> scanBarcode() async {
    String url;
    try {
      url = await FlutterBarcodeScanner.scanBarcode();
      if (url != '-1') {
        launchURL(url);
      }
      setState(() {
        _text = '';
      });
    } on PlatformException {
      url = 'Failed to get platform version.';
    }
  }

  Future launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }

    setState(() {
      _text = url;
    });
  }
}
