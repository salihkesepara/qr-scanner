import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FloatingActionButtons extends StatelessWidget {
  final String text;
  final Function scanBarcode;

  const FloatingActionButtons({Key key, this.text, this.scanBarcode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        FloatingActionButton.extended(
          label: Text('Copy', style: TextStyle(fontSize: 11)),
          tooltip: 'Copy',
          icon: Icon(Icons.content_copy, size: 18),
          onPressed: _handleCopyOnPressed,
        ),
        SizedBox(height: 16),
        FloatingActionButton.extended(
          label: Text('Scan'),
          tooltip: 'QR Code Scanner',
          icon: ImageIcon(
            AssetImage('assets/images/qrcode.png'),
            size: 32,
            color: Colors.black,
          ),
          onPressed: _handleScanOnPressed,
        ),
      ],
    );
  }

  void _handleCopyOnPressed() {
    if (text.isEmpty) return;
    
    Clipboard.setData(ClipboardData(text: text));

    Fluttertoast.showToast(
      msg: "Text copied to clipboard.",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Colors.grey,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  void _handleScanOnPressed() {
    scanBarcode();
  }
}
